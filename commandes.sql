CREATE TABLE commandes (
id_commande SERIAL PRIMARY KEY,
nom VARCHAR(100),
descrip VARCHAR(250)
);

INSERT INTO commandes(nom, descrip) 
VALUES ( 'pwd', 'Imprime sur lécran de la ligne de commandes le répertoire de travail, cest-à-dire le répertoire ou le dossier que vous êtes actuellement.') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'ls', 'Imprime tous les fichiers dans le répertoire de travail.') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'su', 'Entrez le mot de passe root: La commande SU donne à lutilisateur la racine des privilèges.') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'apt-get [package]', 'Appelle le Gestionnaire de paquets qui permet de télécharger des logiciels en paquets.') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'nano [file]', 'Nano est un éditeur de texte de base qui fonctionne dans la Shell. Il vous permet déditer des documents avec les privilèges root si vous avez exécuté SU.') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'rm [file]', 'Supprime le fichier spécifié.') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'cp [OPTION]... [-T] SOURCE DEST', 'copie de fichier') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'dir [OPTION]... [FILE]...', 'Affiche le contenu dun répertoire') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'mv [OPTION]... [-T] SOURCE DEST', 'Déplace ou renomme des fichiers ou des dossiers') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'mkdir [folder name]', 'création de dossier') ;

INSERT INTO commandes(nom, descrip) 
VALUES ( 'scp [OPTION] [user@]SRC_HOST:]file1 [user@]DEST_HOST:]file2', 'Copie des fichiers entre deux machines au travers dune connexion ssh') ;
