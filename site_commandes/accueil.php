<?php 
echo "return ";
/** Connexion **/
$connexion = new PDO('pgsql:host=postgres_commandes;port=5432;dbname=dbcm', 'api', 'dreamer');	
echo "something";

/** Préparation et exécution de la requête **/
$sql = "SELECT nom, descrip FROM commandes";
$resultset = $connexion->prepare($sql);
$resultset->execute();

/** Traitement du résultat **/
echo '<table border="1">
		<tr>
			<th>Nom de commande</th>
			<th>Description</th>
		</tr>';
while ($row = $resultset->fetch(PDO::FETCH_ASSOC)) {
	echo '<tr>';
	echo '<td>' . $row[nom] . '</td>';
	echo '<td>' . $row[descrip] . '</td>';
	echo '</tr>';
	}
	
echo '</table>';
/** Déconnexion **/
$connexion=null;

?>