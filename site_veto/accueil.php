<?php 
echo "return ";
/** Connexion **/
$connexion = new PDO('pgsql:host=postgres_veto;port=5432;dbname=mydb', 'api', 'dreamer');	
echo "something";

/** Préparation et exécution de la requête **/
$sql = "SELECT nom, num_puce FROM  animal";
$resultset = $connexion->prepare($sql);
$resultset->execute();

/** Traitement du résultat **/
echo '<table border="1">
		<tr>
			<th>Nom</th>
			<th>Num_puce</th>
		</tr>';
while ($row = $resultset->fetch(PDO::FETCH_ASSOC)) {
	echo '<tr>';
	echo '<td>' . $row[nom] . '</td>';
	echo '<td>' . $row[num_puce] . '</td>';
	echo '</tr>';
	}
	
echo '</table>';
/** Déconnexion **/
$connexion=null;

?>